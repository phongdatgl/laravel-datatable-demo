<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{

		return View::make('hello');
	}
	public function datatables()
	{
		$result = User::select('id', 'username', 'first_name', 'created_at');

		return Datatables::of($result)
			->add_column('edit', '<a href="/admin/users/{{ $id }}/edit"><i class="icon-list-alt"></i>Edit</a>')
			->make();
	}

}
