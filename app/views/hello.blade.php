<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>User Manager</title>

		<!-- Bootstrap CSS -->

		{{ HTML::style('//cdn.datatables.net/plug-ins/1.10.6/integration/bootstrap/3/dataTables.bootstrap.css') }}
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
		

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<h1 class="text-center">Hello World</h1>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					
					<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="users">
					<thead>
						<tr>
							<th>ID</th>
							<th>Username</th>
							<th>First Name</th>					
							<th>Created At</th>					
							<th>Action</th>					
						</tr>
					</thead>
					
				</table>
				</div>
			</div>
		</div>
		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>

		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
		{{ HTML::script('//cdn.datatables.net/1.10.6/js/jquery.dataTables.min.js') }}
		{{ HTML::script('//cdn.datatables.net/plug-ins/1.10.6/integration/bootstrap/3/dataTables.bootstrap.js') }}
		<script type="text/javascript">
			$('#users').dataTable( {
				"bProcessing": true,
				"bServerSide": true,
				"sAjaxSource": "{{ URL::route('users.datatables') }}",
				"aaSorting": [[ 0, "desc" ]],
			});
		</script>
	</body>
</html>