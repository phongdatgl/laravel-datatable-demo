<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class FirstTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(101, 205) as $index)
		{
			$user  = User::findOrFail($index);
			$user->first_name = $faker->firstName;
			$user->save();
		}
	}

}